import { unlinkSync } from 'fs';
import { JsonSchema, Model, RelationMappings } from 'objection';
import { join } from 'path';

import { UserDTO } from '../../dto/general/body/user.dto';
import { knex } from '../knex';
import { User } from './user.model';

Model.knex(knex);

export class Image extends Model {
  uuid?: string;
  title?: string;
  desc?: string;
  upvotes?: number;
  format: 'jpg' | 'png';
  createdAt?: string;
  modifiedAt?: string;

  userUuid?: string;
  user?: UserDTO;

  static tableName = 'images';
  static idColumn = 'uuid';
  static jsonSchema: JsonSchema = {
    type: 'object',
    required: ['uuid', 'title', 'userUuid'],
    properties: {
      uuid: { type: 'string', maxLength: 36, minLength: 36 },
      title: { type: 'string', maxLength: 100, minLength: 1 },
      desc: { type: 'string', maxLength: 65535 },
      upvotes: { type: 'integer' },
      createdAt: { type: 'string', maxLength: 19, minLength: 19 },
      modifiedAt: { type: 'string', maxLength: 19, minLength: 19 },
      userUuid: { type: 'string', maxLength: 36, minLength: 36 },
    },
  };
  static relationMappings: RelationMappings = {
    user: {
      relation: Model.BelongsToOneRelation,
      modelClass: User,
      join: {
        from: 'images.userUuid',
        to: 'users.uuid',
      },
    },
  };

  $beforeInsert() {
    this.upvotes = 0;
    this.createdAt = this.getDBTimestamp();
    this.modifiedAt = this.getDBTimestamp();
  }

  $beforeUpdate() {
    this.modifiedAt = this.getDBTimestamp();
  }

  $beforeDelete() {
    unlinkSync(join(__dirname, '..', '..', '..', 'assets', 'images', 'high', this.uuid + '.jpg'));
    unlinkSync(join(__dirname, '..', '..', '..', 'assets', 'images', 'low', this.uuid + '.jpg'));
  }

  private getDBTimestamp() {
    return new Date()
      .toISOString()
      .slice(0, 19)
      .replace('T', ' ');
  }
}
