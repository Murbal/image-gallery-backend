import * as bcrypt from 'bcrypt';
import { JsonSchema, Model, RelationMappings } from 'objection';
import * as uuid5 from 'uuid/v5';

import { knex } from '../knex';
import { Image } from './image.model';

Model.knex(knex);

export class User extends Model {
  uuid?: string;
  username?: string;
  password?: string;
  createdAt?: string;
  modifiedAt?: string;

  images?: Image[] | Object;

  static tableName = 'users';
  static idColumn = 'uuid';
  static jsonSchema: JsonSchema = {
    type: 'object',
    required: ['username', 'password'],
    properties: {
      uuid: { type: 'string', maxLength: 36, minLength: 36 },
      username: { type: 'string', maxLength: 50 },
      password: { type: 'string', maxLength: 60, minLength: 8 },
      createdAt: { type: 'string', maxLength: 19, minLength: 19 },
      modifiedAt: { type: 'string', maxLength: 19, minLength: 19 },
    },
  };
  static modelPaths = [__dirname];
  static relationMappings: RelationMappings = {
    images: {
      relation: Model.HasManyRelation,
      modelClass: Image,
      join: {
        from: 'users.uuid',
        to: 'images.userId',
      },
    },
  };

  $beforeInsert() {
    const namespace = process.env.NAMESPACE;
    this.uuid = uuid5(this.username, namespace);
    const salt = bcrypt.genSaltSync(parseInt(process.env.ROUNDS || '12', 10));
    this.password = bcrypt.hashSync(this.password, salt);
    this.createdAt = this.getDBTimestamp();
    this.modifiedAt = this.getDBTimestamp();
  }

  $beforeUpdate() {
    this.modifiedAt = this.getDBTimestamp();
  }

  private getDBTimestamp() {
    return new Date()
      .toISOString()
      .slice(0, 19)
      .replace('T', ' ');
  }
}
