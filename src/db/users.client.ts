import { Injectable } from '@nestjs/common';

import { UserDTO } from '../dto/general/body/user.dto';
import { ImagesClient } from './images.client';
import { Image } from './models/image.model';
import { User } from './models/user.model';

@Injectable()
export class UsersClient {
  imagesClient: ImagesClient;
  pageSize: number;
  tableName: string;

  constructor() {
    this.imagesClient = new ImagesClient();
    this.pageSize = 20;
    this.tableName = 'users';
  }

  async fetchOneByUuidWithoutImages(uuid: string): Promise<User | null> {
    const user = await User.query()
      .select('uuid', 'username', 'created_at')
      .where('uuid', uuid)
      .first();

    if (!user) {
      return null;
    }

    return user;
  }

  async fetchOneByUuidWithNewestImages(uuid: string, imagesPage: number): Promise<User | null> {
    const user = await this.fetchOneByUuidWithoutImages(uuid);

    if (!user) {
      return null;
    }
    user.images = await this.imagesClient.fetchNewestPaginatedImagesByUserUuid(user.uuid, imagesPage);
    return user;
  }

  async fetchNewestUsersByPageWithoutScores(page: number) {
    const users = await User.query()
      .select('uuid', 'username')
      .orderBy('created_at', 'desc')
      .limit(this.pageSize)
      .offset((page - 1) * this.pageSize);

    if (users.length === 0) {
      return null;
    }

    return users;
  }

  async fetchOldestUsersByPageWithoutScores(page: number) {
    const users = await User.query()
      .select('uuid', 'username')
      .orderBy('created_at', 'asc')
      .limit(this.pageSize)
      .offset((page - 1) * this.pageSize);

    if (users.length === 0) {
      return null;
    }

    return users;
  }

  async fetchUsersByPageWithHighestUpvotes(page: number) {
    const t = Date.now();
    const users = await User.query()
      .select(
        'uuid',
        'username',
        Image.query()
          .sum('upvotes')
          .where('user_uuid', 'users.uuid')
          .as('upvotes'),
      )
      .orderBy('upvotes', 'desc')
      .limit(this.pageSize)
      .offset((page - 1) * this.pageSize);
    console.log(Date.now() - t);
    if (users.length === 0) {
      return null;
    }

    return users;
  }

  async fetchUsersByPageWithLeastUpvotes(page: number) {
    const users = await User.query()
      .select(
        'uuid',
        'username',
        Image.query()
          .sum('upvotes')
          .where('user_uuid', 'users.uuid')
          .as('upvotes'),
      )
      .orderBy('upvotes', 'asc')
      .limit(this.pageSize)
      .offset((page - 1) * this.pageSize);
    console.timeEnd('fetch');
    if (users.length === 0) {
      return null;
    }

    return users;
  }

  async getCount() {
    return await User.query().resultSize();
  }

  async fetchByUsernameForLogin(username: string): Promise<User | null> {
    const user = await User.query()
      .select('uuid', 'username', 'password')
      .where('username', username);

    if (user.length === 0) {
      return null;
    }

    return user[0];
  }

  async insertUser(user: UserDTO): Promise<User> {
    user.profilePictureBase64 = undefined;
    const insertedUser = await User.query().insertAndFetch(user as Partial<User>);

    if (!insertedUser) {
      return null;
    }

    return insertedUser;
  }
}
