import { Injectable } from '@nestjs/common';

import { ImageDTO } from '../dto/general/body/image.dto';
import { Image } from './models/image.model';
import { QueryBuilder } from 'objection';

@Injectable()
export class ImagesClient {
  pageSize: number;
  tableName: string;

  constructor() {
    this.pageSize = 10;
    this.tableName = 'images';
  }

  async fetchNewestPaginatedImagesByUserUuid(uuid: string, pageNum: number): Promise<Image[]> {
    return this.getPaginatedByUuidQuery(uuid, pageNum).orderBy('created_at', 'desc');
  }

  async fetchOldestPaginatedImagesByUserUuid(uuid: string, pageNum: number): Promise<Image[]> {
    return this.getPaginatedByUuidQuery(uuid, pageNum).orderBy('created_at', 'asc');
  }

  async fetchMostUpvotesPaginatedImagesByUserUuid(uuid: string, pageNum: number): Promise<Image[]> {
    return this.getPaginatedByUuidQuery(uuid, pageNum).orderBy('upvotes', 'desc');
  }

  async fetchLeastUpvotesPaginatedImagesByUserUuid(uuid: string, pageNum: number): Promise<Image[]> {
    return this.getPaginatedByUuidQuery(uuid, pageNum).orderBy('upvotes', 'asc');
  }

  private getPaginatedByUuidQuery(
    uuid: string,
    pageNum: number,
    getCreatedAt?: boolean,
  ): QueryBuilder<Image, Image[], Image[]> {
    const columnNamesToGet = ['uuid', 'title', 'desc', 'upvotes', 'format'];

    if (getCreatedAt) {
      columnNamesToGet.push('created_at');
    }

    return Image.query()
      .select(...columnNamesToGet)
      .where('user_uuid', uuid)
      .limit(this.pageSize)
      .offset((pageNum - 1) * this.pageSize);
  }

  async getCount(uuid: string) {
    return await Image.query()
      .where('user_uuid', uuid)
      .resultSize();
  }

  async addImageToUser(image: ImageDTO): Promise<Image> {
    image.base64 = undefined;
    return await Image.query().insert(image as Partial<Image>);
  }

  async incrementUpvotesOfImage(imageUuid: string) {
    return Image.query()
      .increment('upvotes', 1)
      .where('uuid', imageUuid);
  }

  async decrementUpvotesOfImage(imageUuid: string) {
    return Image.query()
      .decrement('upvotes', 1)
      .where('uuid', imageUuid);
  }
}
