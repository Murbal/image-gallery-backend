import { BadRequestException, Injectable, NotFoundException } from '@nestjs/common';
import { join } from 'path';
import * as sharp from 'sharp';
import { promisify } from 'util';
import * as uuid4 from 'uuid/v4';

import { ImagesClient } from '../db/images.client';
import { Image } from '../db/models/image.model';
import { ImageDTO } from '../dto/general/body/image.dto';
import { VotesQueryDTO } from '../dto/general/query/votes.query.dto';
import { GetImagesByPagesAndSortQueryDTO } from '../dto/general/query/getImagesByPageAndSort.query.dto';

@Injectable()
export class ImagesService {
  constructor(private readonly client: ImagesClient) {}

  async addImage(image: ImageDTO): Promise<Image> {
    const supportedFormats = ['jpg', 'png'];
    if (!supportedFormats.includes(image.format.toLowerCase())) {
      throw new BadRequestException(`${image.format} images are not allowed`);
    }

    const filename = uuid4();
    const normalResFilePath = join(
      __dirname,
      '..',
      '..',
      'assets',
      'images',
      'high',
      `${filename}.${image.format}`,
    );
    await sharp(Buffer.from(image.base64, 'base64')).toFile(normalResFilePath);

    const sizeOf = promisify(require('image-size'));
    const dimensions = await sizeOf(normalResFilePath);
    await sharp(Buffer.from(image.base64, 'base64'))
      .resize(Math.round((dimensions.width * 50) / 100))
      .toFile(join(__dirname, '..', '..', 'assets', 'images', 'low', `${filename}.${image.format}`));

    image.uuid = filename;

    return this.client.addImageToUser(image);
  }

  async getImagesByPageAndSort(uuid: string, n: number, q: GetImagesByPagesAndSortQueryDTO) {
    let images: Image[];
    const sort= q.date ? 'date' + q.date : 'votes' + q.upvotes;

    switch (sort) {
      case 'datenewest':
        images = await this.client.fetchNewestPaginatedImagesByUserUuid(uuid, n);
        break;

      case 'dateoldest':
        images = await this.client.fetchOldestPaginatedImagesByUserUuid(uuid, n);
        break;

      case 'votesmost':
        images = await this.client.fetchMostUpvotesPaginatedImagesByUserUuid(uuid, n);
        break;

      case 'voteleast':
        images = await this.client.fetchLeastUpvotesPaginatedImagesByUserUuid(uuid, n);
        break;

      default:
        throw new BadRequestException('Specify either upvotes or date query parameter');
    }

    return this.preparePaginatedRes(n, images, uuid);
  }

  private async preparePaginatedRes(n: number, images: Image[], uuid: string) {
    return {
      data: images,
      metadata: await this.getPaginationMetadata(n, uuid),
    };
  }

  private async getPaginationMetadata(n: number, uuid: string) {
    const amount: number = await this.client.getCount(uuid);
    const pageBeginCount: number = (n - 1) * this.client.pageSize;
    const pageEndCount: number = n * this.client.pageSize;
    let hasNext: boolean = false;
    let hasPrevious: boolean = false;

    if (amount > pageEndCount) {
      hasNext = true;
    }

    if (pageBeginCount !== 0) {
      hasPrevious = true;
    }

    return {
      hasNext,
      hasPrevious,
      totalItems: amount,
      totalPages: amount > 0 ? Math.trunc(amount / this.client.pageSize) + 1 : 0,
    };
  }

  async alterVotesOfImage(imageUuid: string, q: VotesQueryDTO): Promise<Image[]> {
    if (!q.increment && !q.decrement) {
      throw new BadRequestException('Specify either increment or decrement query parameter');
    }

    if (q.increment && q.decrement) {
      throw new BadRequestException('Specify either increment or decrement as true, not both');
    }

    if (q.increment) {
      return this.client.incrementUpvotesOfImage(imageUuid);
    }

    if (q.decrement) {
      return this.client.decrementUpvotesOfImage(imageUuid);
    }
  }
}
