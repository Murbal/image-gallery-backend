import {
  Body,
  Controller,
  ForbiddenException,
  Get,
  NotFoundException,
  Param,
  Patch,
  Post,
  Query,
  Request,
  Res,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { Response } from 'express';
import { existsSync } from 'fs';
import * as jwt from 'jsonwebtoken';
import { join } from 'path';
import * as uuid5 from 'uuid/v5';

import { ImageDTO } from '../dto/general/body/image.dto';
import { VotesQueryDTO } from '../dto/general/query/votes.query.dto';
import { UuidDTO } from '../dto/general/param/uuid.dto';
import { ImagesService } from './images.service';

@Controller('images')
export class ImagesController {
  constructor(private readonly imagesService: ImagesService) {}

  // @Get('high/:name') // Dev mode
  // async sendHighImage(@Res() res: Response, @Param('name') name: string) {
  //   const filename = join(__dirname, '..', '..', 'assets', 'images', 'high', name);

  //   console.log(name, existsSync(join(__dirname, '..', '..', 'assets', 'images', 'high', name)));
  //   if (existsSync(filename)) {
  //     res.sendFile(filename);
  //   } else {
  //     throw new NotFoundException();
  //   }
  // }

  // @Get('low/:name') // Dev mode
  // async sendLowImage(@Res() res: Response, @Param('name') name: string) {
  //   const filename = join(__dirname, '..', '..', 'assets', 'images', 'low', name);

  //   console.log(name, existsSync(join(__dirname, '..', '..', 'assets', 'images', 'high', name)));
  //   if (existsSync(filename)) {
  //     res.sendFile(filename);
  //   } else {
  //     throw new NotFoundException();
  //   }
  // }

  // @Get('profiles/high/:name') // Dev mode
  // async sendHighProfileImage(@Res() res: Response, @Param('name') name: string) {
  //   const filename = join(__dirname, '..', '..', 'assets', 'profiles', 'high', name);

  //   console.log(name, existsSync(join(__dirname, '..', '..', 'assets', 'images', 'high', name)));
  //   if (existsSync(filename)) {
  //     res.sendFile(filename);
  //   } else {
  //     throw new NotFoundException();
  //   }
  // }

  // @Get('profiles/low/:name') // Dev mode
  // async sendLowProfileImage(@Res() res: Response, @Param('name') name: string) {
  //   const filename = join(__dirname, '..', '..', 'assets', 'profiles', 'low', name);

  //   console.log(name, existsSync(join(__dirname, '..', '..', 'assets', 'images', 'high', name)));
  //   if (existsSync(filename)) {
  //     res.sendFile(filename);
  //   } else {
  //     throw new NotFoundException();
  //   }
  // }

  @Post()
  @UseGuards(AuthGuard())
  async addImage(@Body() body: ImageDTO, @Request() req) {
    const decoded = jwt.decode(req.user) as { [key: string]: any };
    const uuidOfDecodedUsername = uuid5(decoded.username, process.env.NAMESPACE);

    if (uuidOfDecodedUsername !== body.userUuid) {
      throw new ForbiddenException('You cannot add an image to another users account');
    }

    return this.imagesService.addImage(body);
  }

  @Patch(':uuid/upvotes')
  @UseGuards(AuthGuard())
  async alterUpvotesOfImage(@Param() param: UuidDTO, @Query() q: VotesQueryDTO) {
    return this.imagesService.alterVotesOfImage(param.uuid, q);
  }
}
