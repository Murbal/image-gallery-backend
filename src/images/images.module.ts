import { CacheInterceptor, CacheModule, Module } from '@nestjs/common';
import { APP_INTERCEPTOR } from '@nestjs/core';
import { JwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';

import { ImagesClient } from '../db/images.client';
import { UsersClient } from '../db/users.client';
import { ImagesController } from './images.controller';
import { ImagesService } from './images.service';

@Module({
  imports: [
    CacheModule.register(),
    PassportModule.register({ defaultStrategy: 'jwt' }),
    JwtModule.register({
      secretOrPrivateKey: process.env.JWT_SECRET || 'secretKey',
      signOptions: {
        expiresIn: parseInt(process.env.JWT_EXP, 10) || 3600,
      },
    }),
  ],
  controllers: [ImagesController],
  providers: [
    ImagesService,
    {
      provide: APP_INTERCEPTOR,
      useClass: CacheInterceptor,
    },
    ImagesClient,
    UsersClient,
  ],
  exports: [ImagesService],
})
export class ImagesModule {}
