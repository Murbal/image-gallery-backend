import { HttpException, HttpStatus, Injectable, NotFoundException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';

import JwtPayloadDTO from '../dto/auth/jwt.payload.dto';
import { UsersService } from '../users/users.service';

@Injectable()
export class AuthService {
  constructor(private readonly usersService: UsersService, private readonly jwtService: JwtService) {}

  async validateUser(user: JwtPayloadDTO): Promise<string> {
    if (await this.usersService.verifyCredentials(user.username, user.password)) {
      return this.jwtService.sign({ username: user.username, password: user.password });
    }

    throw new NotFoundException('No user found in database for JWT validation');
  }
}
