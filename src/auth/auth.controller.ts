import { Body, Controller, Post } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';

import { UserDTO } from '../dto/general/body/user.dto';
import { UsersService } from '../users/users.service';
import { AuthService } from './auth.service';
import * as redis from 'redis';

@Controller('auth')
export class AuthController {
  redisClient: redis.RedisClient;
  constructor(
    private readonly authService: AuthService,
    private readonly usersService: UsersService,
    private readonly jwtService: JwtService,
  ) {
    this.redisClient = redis.createClient(6379, 'localhost');
  }

  @Post('register')
  async register(@Body() body: UserDTO) {
    const user = await this.usersService.insertUser(body);
    const token = this.jwtService.sign({ username: body.username, password: body.password });
    return { ...user, token };
  }

  @Post('login')
  async login(@Body() body: UserDTO) {
    return await this.authService.validateUser(body);
  }
}
