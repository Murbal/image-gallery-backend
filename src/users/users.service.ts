import { BadRequestException, Injectable, NotFoundException } from '@nestjs/common';
import * as bcrypt from 'bcrypt';
import { join } from 'path';
import * as sharp from 'sharp';
import { promisify } from 'util';
import * as uuid5 from 'uuid/v5';

import { User } from '../db/models/user.model';
import { UsersClient } from '../db/users.client';
import { UserDTO } from '../dto/general/body/user.dto';
import { ImagesService } from '../images/images.service';
import { GetOneUserByIdQueryDTO } from '../dto/general/query/getOneUserById.query.dto';

@Injectable()
export class UsersService {
  constructor(private readonly client: UsersClient, private readonly imagesService: ImagesService) {}

  async getOneByUuid(uuid: string, q: GetOneUserByIdQueryDTO, req): Promise<User> {
    if (q.withScores == 'true') {
      if (!q.pageNum) {
        throw new BadRequestException(
          "When query param 'withScores' is set to true, you also need to set param 'pageNum'",
        );
      }

      if (!q.date && !q.upvotes) {
        throw new BadRequestException(
          "When query param 'withScores' is set to true, you also need to set param 'date' or 'upvotes'",
        );
      }

      const user = await this.client.fetchOneByUuidWithoutImages(uuid);
      if (!user) {
        throw new NotFoundException();
      }

      user['images'] = await this.imagesService.getImagesByPageAndSort(uuid, q.pageNum, q);
      return user;
    }
    const user = await this.client.fetchOneByUuidWithoutImages(uuid);
    if (!user) {
      throw new NotFoundException();
    }
    return user;
  }

  async getUsersByPageAndSort(n: number, q: any) {
    if (Object.keys(q).length === 0) {
      throw new BadRequestException('Specify query');
    }

    const url = `${process.env.API_HOST}/users/pages/`;
    const sort = q.date ? 'date ' + q.date : 'votes ' + q.upvotes;
    let users: User[];

    switch (sort) {
      case 'date newest':
        users = await this.client.fetchNewestUsersByPageWithoutScores(n);
        break;

      case 'date oldest':
        users = await this.client.fetchOldestUsersByPageWithoutScores(n);
        break;

      case 'votes most':
        users = await this.client.fetchUsersByPageWithHighestUpvotes(n);
        break;

      case 'votes least':
        users = await this.client.fetchUsersByPageWithLeastUpvotes(n);
        break;

      default:
        throw new BadRequestException('Specify either upvotes or date query parameter');
    }

    if (!users) {
      throw new NotFoundException();
    }

    return this.preparePaginatedRes(n, users, url);
  }

  async preparePaginatedRes(n, users, url) {
    const [hasNext, hasPrevious] = await this.getNextAndPrevious(n);
    if (hasNext && hasPrevious) {
      return { users: users, previous: url + (n - 1), next: url + (n + 1) };
    }
    if (hasNext && !hasPrevious) {
      return { users: users, next: url + (n + 1) };
    }
    if (!hasNext && hasPrevious) {
      return { users: users, previous: url + (n - 1) };
    }
    return { users: users };
  }

  async getNextAndPrevious(n) {
    const amount = await this.client.getCount();
    const pageBeginCount = (n - 1) * this.client.pageSize;
    const pageEndCount = n * this.client.pageSize;
    let hasNext = false;
    let hasPrevious = false;
    if (amount > pageEndCount) {
      hasNext = true;
    }
    if (pageBeginCount !== 0) {
      hasPrevious = true;
    }
    return [hasNext, hasPrevious];
  }

  async insertUser(user: UserDTO): Promise<User> {
    if (user.profilePictureBase64) {
      const sizeOf = promisify(require('image-size'));
      const filename = uuid5(user.username, process.env.NAMESPACE) + '.jpg';
      const normalResFilePath = join(__dirname, '..', '..', 'assets', 'profiles', 'high', filename);
      await sharp(Buffer.from(user.profilePictureBase64, 'base64')).toFile(normalResFilePath);

      const dimensions = await sizeOf(normalResFilePath);
      await sharp(Buffer.from(user.profilePictureBase64, 'base64'))
        .resize(Math.round(dimensions.width * 30) / 100)
        .toFile(join(__dirname, '..', '..', 'assets', 'profiles', 'low', filename));
    }
    const insertedUser = await this.client.insertUser(user);
    if (!insertedUser) {
      throw new BadRequestException();
    }
    return insertedUser;
  }

  async verifyCredentials(username: string, password: string): Promise<boolean> {
    const userOrNull = await this.client.fetchByUsernameForLogin(username);
    if (!userOrNull) {
      return false;
    }
    if (bcrypt.compareSync(password, (userOrNull as User).password)) {
      return true;
    }
    return false;
  }
}
