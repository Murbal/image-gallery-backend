import { CacheInterceptor, CacheModule, Module } from '@nestjs/common';
import { APP_INTERCEPTOR } from '@nestjs/core';
import { JwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';

import { UsersClient } from '../db/users.client';
import { UsersController } from './users.controller';
import { UsersService } from './users.service';
import { ImagesService } from '../images/images.service';
import { ImagesModule } from '../images/images.module';

@Module({
  imports: [
    PassportModule.register({ defaultStrategy: 'jwt' }),
    JwtModule.register({
      secretOrPrivateKey: process.env.JWT_SECRET || 'secretKey',
      signOptions: {
        expiresIn: parseInt(process.env.JWT_EXP, 10) || 3600,
      },
    }),
    ImagesModule,
  ],
  controllers: [UsersController],
  providers: [UsersService, UsersClient],
  exports: [UsersService],
})
export class UsersModule {}
