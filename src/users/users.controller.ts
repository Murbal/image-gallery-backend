import {
  Controller,
  Get,
  Param,
  ParseIntPipe,
  Query,
  UseGuards,
  Request,
  UseInterceptors,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import * as redis from 'redis';
import { UuidDTO } from '../dto/general/param/uuid.dto';
import { UsersService } from './users.service';
import { GetOneUserByIdQueryDTO } from '../dto/general/query/getOneUserById.query.dto';

@Controller('users')
export class UsersController {
  redisClient: redis.RedisClient;
  constructor(private readonly usersService: UsersService) {
    this.redisClient = redis.createClient(6379, 'localhost');
  }

  getCached(key: string): Promise<string | Error> {
    return new Promise<string | Error>((resolve, reject) => {
      this.redisClient.get(key, (err: Error, res: string) => {
        if (err) {
          reject(err);
        } else {
          resolve(res);
        }
      });
    });
  }

  cache(key: string, valToCache: any) {
    this.redisClient.set(key, JSON.stringify(valToCache));
  }

  @Get(':uuid')
  @UseGuards(AuthGuard())
  async getOneById(@Param() uuidDto: UuidDTO, @Query() q: GetOneUserByIdQueryDTO, @Request() req) {
    const key = 'oneUserByUuid:' + uuidDto.uuid + ':' + req.query;
    const cachedVal = await this.getCached(key);

    if (cachedVal) {
      return cachedVal;
    }

    const user = await this.usersService.getOneByUuid(uuidDto.uuid, q, req);

    this.cache(key, user);

    return user;
  }

  @Get('pages/:n')
  @UseGuards(AuthGuard())
  async getUsersByPageAndSort(@Param('n', new ParseIntPipe()) page: number, @Query() q: any) {
    const key = 'newestUsersPage:' + page;
    const cachedVal = await this.getCached(key);

    if (cachedVal) {
      return cachedVal;
    }

    const users = await this.usersService.getUsersByPageAndSort(page, q);

    this.cache(key, users);

    return users;
  }
}
