import { IsDefined, IsString, Length } from 'class-validator';

export default class JwtPayloadDTO {
  @IsDefined()
  @IsString()
  @Length(1, 50)
  username: string;
  @IsDefined()
  @IsString()
  @Length(8, 60)
  password: string;
}
