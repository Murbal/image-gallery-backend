import {IsIn, IsOptional, IsBooleanString, IsInt, IsPositive} from 'class-validator'

export class GetOneUserByIdQueryDTO {
	@IsIn(["most", "least"])
	@IsOptional()
	upvotes: string
	@IsIn(["newest", "oldest"])
	@IsOptional()
	date: string
	@IsOptional()
	@IsBooleanString()
	withScores: string
	@IsInt()
	@IsPositive()
	@IsOptional()
	pageNum: number
}