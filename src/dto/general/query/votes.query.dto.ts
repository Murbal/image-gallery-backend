import { IsBoolean, IsBooleanString, IsOptional } from 'class-validator';

export class VotesQueryDTO {
  @IsBooleanString()
  @IsOptional()
  increment: boolean;
  @IsBooleanString()
  @IsOptional()
  decrement: boolean;
}
