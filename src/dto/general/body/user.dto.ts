import { IsBase64, IsOptional, IsString, Length } from 'class-validator';

export class UserDTO {
  @IsString()
  @Length(1, 50)
  username: string;
  @IsString()
  @Length(8, 60)
  password: string;
  @IsBase64()
  @IsOptional()
  profilePictureBase64?: string;
}
