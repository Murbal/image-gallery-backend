import { IsBase64, IsOptional, IsString, IsUUID, Length } from 'class-validator';

import { UserDTO } from './user.dto';

export class ImageDTO {
  uuid?: string;
  @IsString()
  @Length(1, 100)
  title: string;
  @IsString()
  @Length(1, 65535)
  @IsOptional()
  desc?: string;
  @IsBase64()
  @IsOptional()
  base64: string;
  @IsUUID()
  userUuid: UserDTO | string;
  @IsString()
  format: string;
}
