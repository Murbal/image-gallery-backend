import { UsersClient } from '../../src/db/users.client';
import * as chai from 'chai';
import * as chaiAsPromised from 'chai-as-promised';
import * as randomstring from 'randomstring';
import * as bcrypt from 'bcrypt';
import * as uuid5 from 'uuid/v5';
import * as dotenv from 'dotenv';
import { ImagesClient } from '../../src/db/images.client';
import { ImageDTO } from '../../src/dto/general/body/image.dto';
import { UserDTO } from '../../src/dto/general/body/user.dto';
import { User } from '../../src/db/models/user.model';

dotenv.config();

const expect = chai.expect;

chai.use(chaiAsPromised);

const usersClient = new UsersClient();
const imagesClient = new ImagesClient();

function insertValidUser(): Promise<User> {
  const userToInsert: UserDTO = {
    username: randomstring.generate(12),
    password: randomstring.generate(12),
  };
  return usersClient.insertUser(userToInsert);
}

describe('images client', () => {
  describe('insert', () => {
    describe('success', () => {
      it('should insert and return correct image', async () => {
        const insertedUser = await insertValidUser();
        const imageToInsert: ImageDTO = {
          userUuid: insertedUser.uuid,
          title: randomstring.generate(12),
          base64: randomstring.generate(1),
          uuid: uuid5(randomstring.generate(12), process.env.NAMESPACE),
          format: 'jpg',
        };
        const image = await imagesClient.addImageToUser(imageToInsert);
        expect(image.userUuid).to.equal(imageToInsert.userUuid);
        expect(image.title).to.equal(imageToInsert.title);
        expect(image.createdAt).to.exist;
        expect(image.modifiedAt).to.exist;
        expect(image.desc).to.not.exist;
        expect(image.upvotes).to.equal(0);
      });

      it('should insert and return correct image', async () => {
        const insertedUser = await insertValidUser();
        const imageToInsert: ImageDTO = {
          userUuid: insertedUser.uuid,
          desc: randomstring.generate(12),
          title: randomstring.generate(12),
          base64: randomstring.generate(1),
          uuid: uuid5(randomstring.generate(12), process.env.NAMESPACE),
          format: 'jpg',
        };
        const image = await imagesClient.addImageToUser(imageToInsert);
        expect(image.userUuid).to.equal(imageToInsert.userUuid);
        expect(image.title).to.equal(imageToInsert.title);
        expect(image.createdAt).to.exist;
        expect(image.modifiedAt).to.exist;
        expect(image.desc).to.equal(imageToInsert.desc);
        expect(image.upvotes).to.equal(0);
      });
    });

    describe('fail', () => {
      it('should fail because of too long title', async () => {
        const insertedUser = await insertValidUser();
        const imageToInsert: ImageDTO = {
          userUuid: insertedUser.uuid,
          title: randomstring.generate(101),
          desc: randomstring.generate(12),
          base64: randomstring.generate(1),
          uuid: uuid5(randomstring.generate(12), process.env.NAMESPACE),
          format: 'jpg',
        };
        expect(imagesClient.addImageToUser(imageToInsert)).to.be.rejected;
      });

      it('should fail because of too long desc', async () => {
        const insertedUser = await insertValidUser();
        const imageToInsert: ImageDTO = {
          userUuid: insertedUser.uuid,
          title: randomstring.generate(101),
          desc: randomstring.generate(65536),
          base64: randomstring.generate(1),
          uuid: uuid5(randomstring.generate(12), process.env.NAMESPACE),
          format: 'jpg',
        };
        expect(imagesClient.addImageToUser(imageToInsert)).to.be.rejected;
      });
    });
  });
});
