import { UsersClient } from '../../src/db/users.client';
import * as chai from 'chai';
import * as chaiAsPromised from 'chai-as-promised';
import * as randomstring from 'randomstring';
import * as bcrypt from 'bcrypt';
import * as uuid5 from 'uuid/v5';
import * as dotenv from 'dotenv';

dotenv.config();

const expect = chai.expect;

chai.use(chaiAsPromised);

const u = new UsersClient();

describe('user client', () => {
  describe('insert', () => {
    describe('success', () => {
      it('should insert correctly', async () => {
        const username = randomstring.generate(8);
        const password = randomstring.generate(8);
        const user = await u.insertUser({
          username,
          password,
        });
        expect(user.username).to.equal(username);
        expect(bcrypt.compareSync(password, user.password)).to.equal(true);
        expect(user.uuid).to.equal(uuid5(user.username, process.env.NAMESPACE));
      });
      it('should insert correctly', async () => {
        const username = randomstring.generate(50);
        const password = randomstring.generate(60);
        const user = await u.insertUser({
          username,
          password,
        });
        expect(user.username).to.equal(username);
        expect(bcrypt.compareSync(password, user.password)).to.equal(true);
        expect(user.uuid).to.equal(uuid5(user.username, process.env.NAMESPACE));
      });
    });
    describe('fail', () => {
      it('should fail because of too short password', async () => {
        const username = randomstring.generate(12);
        const password = randomstring.generate(7);
        expect(
          u.insertUser({
            username,
            password,
          }),
        ).to.be.rejected;
      });
      it('should fail because of too long password', async () => {
        const username = randomstring.generate(12);
        const password = randomstring.generate(61);
        expect(
          u.insertUser({
            username,
            password,
          }),
        ).to.be.rejected;
      });
      it('should fail because of too long username', async () => {
        const username = randomstring.generate(51);
        const password = randomstring.generate(12);
        expect(
          u.insertUser({
            username,
            password,
          }),
        ).to.be.rejected;
      });
    });
  });
});
