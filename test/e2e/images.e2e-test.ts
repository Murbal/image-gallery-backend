import { INestApplication } from '@nestjs/common';
import { Test } from '@nestjs/testing';
import * as chai from 'chai';
import * as chaiPromised from 'chai-as-promised';
import chaiHttp = require('chai-http');
import * as dotenv from 'dotenv';
import * as fs from 'fs';
import * as sizeOf from 'image-size';
import { join } from 'path';
import * as randomstring from 'randomstring';
import { promisify } from 'util';
import * as jwt from 'jsonwebtoken';

import { AuthModule } from '../../src/auth/auth.module';
import { ImagesClient } from '../../src/db/images.client';
import { Image } from '../../src/db/models/image.model';
import { User } from '../../src/db/models/user.model';
import { UsersClient } from '../../src/db/users.client';
import { ImageDTO } from '../../src/dto/general/body/image.dto';
import { UserDTO } from '../../src/dto/general/body/user.dto';
import { ImagesModule } from '../../src/images/images.module';
import { ImagesService } from '../../src/images/images.service';
import { UsersService } from '../../src/users/users.service';
import { FastifyAdapter } from '@nestjs/platform-fastify';

dotenv.config();

chai.use(chaiHttp);
chai.use(chaiPromised);
const expect = chai.expect;

async function imageInsert(format, user, base64, app, token) {
  const image: ImageDTO = {
    userUuid: user.uuid,
    title: randomstring.generate(12),
    desc: randomstring.generate(12),
    format,
    base64,
  };
  const res = await chai
    .request(app.getHttpServer())
    .post('/images')
    .set('Authorization', `Bearer ${token}`)
    .send(image);
  const body = res.body;

  expect(body).to.have.all.keys(
    'uuid',
    'title',
    'desc',
    'upvotes',
    'createdAt',
    'modifiedAt',
    'format',
    'userUuid',
  );
  expect(body.uuid).to.have.length(36);
  expect(body.title).to.equal(image.title);
  expect(body.desc).to.eq(image.desc);
  expect(body.upvotes).to.equal(0);
  expect(body.createdAt).to.have.length(19);
  expect(body.modifiedAt).to.have.length(19);

  return new Promise((resolve, reject) => resolve(body));
}

describe('Images /images', () => {
  let app: INestApplication;

  let usersService: UsersService;
  let imagesService: ImagesService;

  let user: User;
  let user1Dto: UserDTO;
  let user2Dto: UserDTO;
  let imageDto: ImageDTO;
  let imageJpg: Image;
  let imagePng: Image;
  let base64: string;
  let token: string;

  before(async function() {
    this.timeout(150000);
    const module = await Test.createTestingModule({
      imports: [ImagesModule, AuthModule],
    }).compile();
    app = module.createNestApplication(new FastifyAdapter());
    await app.init();
    imagesService = new ImagesService(new ImagesClient());
    usersService = new UsersService(new UsersClient(), imagesService);
    user1Dto = {
      username: randomstring.generate(12),
      password: randomstring.generate(12),
    };
    user2Dto = {
      username: randomstring.generate(12),
      password: randomstring.generate(12),
    };

    user = await usersService.insertUser(user1Dto);
    await usersService.insertUser(user2Dto);
    token = (await chai
      .request(app.getHttpServer())
      .post('/auth/login')
      .send(user1Dto)).text;

    await chai
      .request(app.getHttpServer())
      .post('/auth/login')
      .send(user2Dto);
    base64 = (await promisify(fs.readFile)(join(__dirname, 'sampleImageBase64'))).toString();

    imageDto = {
      userUuid: user.uuid,
      title: randomstring.generate(12),
      desc: randomstring.generate(12),
      format: 'jpg',
      base64,
    };

    imageJpg = await imagesService.addImage(imageDto);
    imagePng = await imagesService.addImage({
      userUuid: user.uuid,
      title: randomstring.generate(12),
      desc: randomstring.generate(12),
      format: 'png',
      base64,
    });
  });
  describe('GET /', () => {
    describe('high/:name', () => {
      describe('succeed', () => {
        it('should return correct normal res image (jpg and png)', async () => {
          const dOriginal = sizeOf(Buffer.from(base64, 'base64'));
          const resJpg = await chai.request(app.getHttpServer()).get(`/images/high/${imageJpg.uuid}.jpg`);
          const dJpg = sizeOf(Buffer.from(resJpg.body, 'base64'));
          expect(dJpg.width).to.equal(dOriginal.width);
          expect(dJpg.height).to.equal(dOriginal.height);

          const resPng = await chai.request(app.getHttpServer()).get(`/images/high/${imagePng.uuid}.png`);
          const dPng = sizeOf(Buffer.from(resPng.body, 'base64'));
          expect(dPng.width).to.equal(dOriginal.width);
          expect(dPng.height).to.equal(dOriginal.height);

          return new Promise((resolve, reject) => resolve());
        });
      });

      describe('fail', () => {
        it('should fail because of not correct uuid', async () => {
          const res1 = await chai
            .request(app.getHttpServer())
            .get(`/images/high/${randomstring.generate(12)}.jpg`);
          if (res1.clientError || res1.serverError) {
            return new Promise((resolve, reject) => resolve());
          }

          const res2 = await chai
            .request(app.getHttpServer())
            .get(`/images/high/${randomstring.generate(12)}.png`);
          if (res2.clientError || res2.serverError) {
            return new Promise((resolve, reject) => resolve());
          }

          chai.assert.fail();
        });
      });
    });
    describe('low/:name', () => {
      describe('success', () => {
        it('should return correct low res image (jpg and png)', async () => {
          const dOriginal = sizeOf(Buffer.from(base64, 'base64'));

          const resJpg = await chai.request(app.getHttpServer()).get(`/images/low/${imageJpg.uuid}.jpg`);
          const dJpg = sizeOf(resJpg.body);
          expect(dJpg.width).to.equal(dOriginal.width / 2);
          expect(dJpg.height).to.equal(dOriginal.height / 2);

          const resPng = await chai.request(app.getHttpServer()).get(`/images/low/${imagePng.uuid}.png`);
          const dPng = sizeOf(resPng.body);
          expect(dPng.width).to.equal(dOriginal.width / 2);
          expect(dJpg.height).to.equal(dOriginal.height / 2);

          return new Promise((resolve, reject) => resolve());
        });
      });
      describe('fail', () => {
        it('should fail because of not correct uuid', async () => {
          const res1 = await chai
            .request(app.getHttpServer())
            .get(`/images/low/${randomstring.generate(12)}.jpg`);
          if (res1.clientError || res1.serverError) {
            return new Promise((resolve, reject) => resolve());
          }

          const res2 = await chai
            .request(app.getHttpServer())
            .get(`/images/low/${randomstring.generate(12)}.png`);
          if (res2.clientError || res2.serverError) {
            return new Promise((resolve, reject) => resolve());
          }

          chai.assert.fail();
        });
      });
    });
  });

  describe('GET /profiles/', () => {
    describe('high/:name', () => {
      describe('succeed', () => {
        it('should return high res default picture with correct dimensions', async () => {
          const res = await chai.request(app.getHttpServer()).get('/images/profiles/high/default.png');
          const d = sizeOf(res.body);

          expect(d.width).to.equal(720);
          expect(d.height).to.equal(720);

          return new Promise((resolve, reject) => resolve());
        });
      });
    });

    describe('low/:name', () => {
      describe('succeed', () => {
        it('should return low res default picture with correct dimensions', async () => {
          const res = await chai.request(app.getHttpServer()).get('/images/profiles/low/default.png');
          const d = sizeOf(Buffer.from(res.body, 'base64'));

          expect(d.width).to.equal(Math.floor((720 * 20) / 100));
          expect(d.height).to.equal(Math.floor((720 * 20) / 100));

          return new Promise((resolve, reject) => resolve());
        });
      });
    });
  });

  describe('POST /', () => {
    describe('succeed', () => {
      it('should insert jpg image and return correctly', async () => {
        return imageInsert('jpg', user, base64, app, token);
      });

      it('should insert png image and return correctly', async () => {
        return imageInsert('png', user, base64, app, token);
      });
    });
    describe('fail', () => {
      it('should fail because of non existing username in db', async () => {
        const res = await chai
          .request(app.getHttpServer())
          .post('/images')
          .set(
            'Authorization',
            `Bearer ${jwt.sign(
              { username: randomstring.generate(12), password: randomstring.generate(12) },
              process.env.JWT_SECRET || 'secretKey',
              { expiresIn: 3600 },
            )}`,
          )
          .send({ ...imageDto, base64, format: 'jpg' });
        if (res.body.statusCode === 404) {
          return new Promise((resolve, reject) => resolve());
        }
        chai.assert.fail();
      });

      it('should fail because of not matching username and userUuid', async () => {
        const res = await chai
          .request(app.getHttpServer())
          .post('/images')
          .set(
            'Authorization',
            `Bearer ${jwt.sign(
              { username: user2Dto.username, password: user2Dto.password },
              process.env.JWT_SECRET || 'secretKey',
              { expiresIn: 3600 },
            )}`,
          )
          .send({ ...imageDto, base64, format: 'jpg' });
        if (res.body.statusCode === 403) {
          return new Promise((resolve, reject) => resolve());
        }
        chai.assert.fail();
      });
    });
  });
});
