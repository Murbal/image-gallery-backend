import { INestApplication } from '@nestjs/common';
import { Test } from '@nestjs/testing';
import * as chai from 'chai';
import * as chaiPromised from 'chai-as-promised';
import chaiHttp = require('chai-http');
import * as randomstring from 'randomstring';

import { AuthModule } from '../../src/auth/auth.module';
import { UsersClient } from '../../src/db/users.client';
import { UsersModule } from '../../src/users/users.module';
import { UsersService } from '../../src/users/users.service';
import { ImageDTO } from '../../src/dto/general/body/image.dto';
import { readFileSync } from 'fs';
import { join } from 'path';
import { ImagesClient } from '../../src/db/images.client';
import { ImagesService } from '../../src/images/images.service';
import { FastifyAdapter } from '@nestjs/platform-fastify';

async function testAmount(app, token, q, user1) {
  const res = await chai
    .request(app.getHttpServer())
    .get('/users/pages/1')
    .set('Authorization', `Bearer ${token}`)
    .query(q);

  if (res.clientError || res.serverError) {
    console.log(res.text);
    throw new Error('Something went wrong');
  }

  const body = JSON.parse(res.text);

  expect(body.users.length).to.be.greaterThan(0);
  expect(body.users.length).to.be.lessThan(21);

  for (const user of body.users) {
    const keys = Object.keys(user1);

    keys.splice(keys.indexOf('password'), 1);
    keys.splice(keys.indexOf('updatedAt'), 1);
    keys.splice(keys.indexOf('profilePictureBase64'), 1);
    keys.splice(keys.indexOf('createdAt'), 1);

    // if (q.upvotes) {
    //   keys.push('upvotes');
    // }

    expect(Object.keys(user)).to.have.length(keys.length);
  }
}

chai.use(chaiHttp);
chai.use(chaiPromised);
const expect = chai.expect;

const imagesService = new ImagesService(new ImagesClient());
const usersService = new UsersService(new UsersClient(), imagesService);

const userDto = {
  username: randomstring.generate(12),
  password: randomstring.generate(12),
};

const allPromise = Promise.all([
  usersService.insertUser(userDto),
  usersService.insertUser({
    username: randomstring.generate(12),
    password: randomstring.generate(12),
  }),
  usersService.insertUser({
    username: randomstring.generate(12),
    password: randomstring.generate(12),
  }),
]);

allPromise.then(([user1, user2, user3]) => {
  const imageDto: ImageDTO = {
    userUuid: user1.uuid,
    title: randomstring.generate(12),
    format: 'jpg',
    base64: readFileSync(join(__dirname, 'sampleImageBase64')).toString(),
  };
  describe('Users /users', () => {
    let app: INestApplication;
    let token: string;
    before(async () => {
      const module = await Test.createTestingModule({
        imports: [UsersModule, AuthModule],
      }).compile();
      app = module.createNestApplication(new FastifyAdapter());
      await app.init();

      const res = await chai
        .request(app.getHttpServer())
        .post('/auth/login')
        .send(userDto);
      if (res.serverError || res.clientError) {
        throw new Error('Could not login');
      }
      for (const i of Array.apply(5, Array(21))) {
        await imagesService.addImage({ ...imageDto });
      }
      token = res.text;
    });

    describe('/:uuid', () => {
      describe('succeed', () => {
        it('should return correct user without query', async () => {
          const res = await chai
            .request(app.getHttpServer())
            .get(`/users/${user1.uuid}`)
            .set('Authorization', `Bearer ${token}`);
          if (res.clientError || res.serverError) {
            console.log(res.text);
            throw new Error('Something went wrong');
          }

          const body = res.body;
          const keys = Object.keys(user1);
          keys.splice(keys.indexOf('password'), 1);
          keys.splice(keys.indexOf('profilePictureBase64'), 1);
          keys.splice(keys.indexOf('modifiedAt'), 1);

          expect(body).to.have.all.keys(keys);
          keys.forEach(prop => {
            if (prop !== 'modifiedAt' && prop !== 'createdAt') {
              expect(body[prop]).to.equal(user1[prop]);
            }
          });
          return new Promise((resolve, reject) => resolve());
        });

        it('should return correct user with correct images with query', async () => {
          const res = await chai
            .request(app.getHttpServer())
            .get(`/users/${user1.uuid}`)
            .set('Authorization', `Bearer ${token}`)
            .query({ withScores: true, pageNum: 1, date: 'newest' });
          if (res.clientError || res.serverError) {
            console.log(res.text);
            throw new Error('Something went wrong');
          }
          const body = res.body;
          const userKeys = Object.keys(user1);

          userKeys.splice(userKeys.indexOf('password'), 1);
          userKeys.splice(userKeys.indexOf('profilePictureBase64'), 1);
          userKeys.splice(userKeys.indexOf('modifiedAt'), 1);

          console.log(res.body);

          expect(body).to.have.all.keys(userKeys.concat(['images']));

          try {
            expect(body.images.data).to.have.length.greaterThan(0);
          } catch (err) {
            chai.assert.fail();
          }

          expect(body.images).to.include.keys(['data']);
          for (const prop of Object.keys(body)) {
            if (prop !== 'modifiedAt' && prop !== 'createdAt' && prop !== 'images') {
              expect(body[prop]).to.equal(user1[prop]);
            }
          }
          let n = 2;
          while (true) {
            const url = `http://localhost:3000/users/${user1.uuid}/`;
            const res = await chai
              .request(app.getHttpServer())
              .get(`/users/${user1.uuid}`)
              .set('Authorization', `Bearer ${token}`)
              .query({ withScores: true, pageNum: n, date: 'newest' });
            if (!res.body.images.next) {
              break;
            }

            expect(res.body.images.next).to.equal(`${url}?withScores=true&date=newest&pageNum=${n + 1}`);
            expect(res.body.images.previous).to.equal(`${url}?withScores=true&date=newest&pageNum=${n - 1}`);

            n++;
          }
        });
        return new Promise((resolve, reject) => resolve());
      });

      describe('fail', () => {
        it('should fail because no token is specified', async () => {
          const res = await chai.request(app.getHttpServer()).get(`/users/${user1.uuid}`);
          if (res.clientError) {
            return new Promise((resolve, reject) => resolve());
          }
          chai.assert.fail();
        });

        it('should fail because wrong query is specified', async () => {
          const res = await chai
            .request(app.getHttpServer())
            .get(`/users/${user1.uuid}`)
            .query({ withScores: true });
          if (res.clientError) {
            return new Promise((resolve, reject) => resolve());
          }
          chai.assert.fail();
        });

        it('should fail because of wrong uuid', async () => {
          const res = await chai
            .request(app.getHttpServer())
            .get(`/users/${randomstring.generate(12)}`)
            .query({ withScores: true, pageNum: 1 });
          if (res.clientError) {
            return new Promise((resolve, reject) => resolve());
          }
          chai.assert.fail();
        });
      });
    });

    describe('/pages/:n', () => {
      describe('succeed', () => {
        it('should return correct amount of users with query date: newest', async () => {
          return testAmount(app, token, { date: 'newest' }, user1);
        });

        it('should return correct amount of users with query date: oldest', async () => {
          return testAmount(app, token, { date: 'oldest' }, user1);
        });

        it('should return correct amount of users with query upvotes: most', async () => {
          return testAmount(app, token, { upvotes: 'most' }, user1);
        });

        it('should return correct amount of users with query upvotes: least', async () => {
          return testAmount(app, token, { upvotes: 'least' }, user1);
        });

        it('should return correct previous and next links', async () => {
          let res = await chai
            .request(app.getHttpServer())
            .get('/users/pages/1')
            .query({ date: 'newest' })
            .set('Authorization', `Bearer ${token}`);
          if (res.clientError || res.serverError) {
            console.log(res.body);
            throw new Error('Something went wrong');
          }

          expect(res.body).to.not.include.keys(['previous']);

          let n = 2;
          const url = 'http://127.0.0.1:3000/users/pages/';

          expect(res.body.previous).to.not.exist;
          if (res.body.next) {
            expect(res.body.next).to.equal(url + n);
          }
          while (true) {
            res = await chai
              .request(app.getHttpServer())
              .get('/users/pages/' + n)
              .query({ date: 'newest' })
              .set('Authorization', `Bearer ${token}`);
            if (!res.body.next) {
              break;
            }

            expect(res.body.next).to.equal(url + (n + 1));
            if (res.body.previous) {
              expect(res.body.previous).to.equal(url + (n - 1));
            }

            n++;
          }
        });
      });

      describe('fail', () => {});
    });
  });
});
