# Rules for contributing

Use `prettier`  
Use `yarn`  
Use `git flow`

# Env variables

DB_CLIENT  
DB_DATABASE  
DB_USER  
DB_PASSWORD  
JWT_SECRET  
JWT_EXP  
ROUNDS  
PORT

# Installation

```
git clone git@bitbucket.org:Murbal/image-gallery-backend.git
```

```
cd image-gallery-backend
```

```
// For npm
npm install
// For yarn
yarn
```

# Getting started

## Commands

Dev related:

- start:dev - runs the server with hot reloading
- test:unit - runs the unit tests
- test:unit:source - first clears the database, runs the unit tests and then clears the database again
- test:e2e - runs the e2e tests
- test:e2e:source - first clears the database, runs the e2e tests and then clears the database again
- migrate:up - creates the tables
- migrate:down - drops the tables
- migrate:fresh - drops the tables and creates them afterwards
- img:clear:unix - **unix only** deletes all images in assets folder
- source:unix - **unix only** deletes all images in assets folder and recreates database

Production related:

- prestart:prod - use this command before start:prod, it builds the project
- start:prod - starts the builded server

# Endpoints

# For all endpoints

When a route throws a non handled error, a Object gets returned with the properties:

- error - 'Internal Server Error'
- statusCode - 500

Specify your jwt token in the 'Authorization' header like so: `Bearer TOKEN`

When you try to access a route without a jwt token, except:

- /auth/register
- /auth/login
- /images/low/\*
- /images/high/\*
- /images/profiles/low/\*
- /images/profiles/high/\*

A Error with the following properties gets returned:

- message - 'Unauthorized'
- statusCode - 401

When you specify a user in the JWT payload that doesn't exist, a error with the following properties gets returned:

- message - 'No user found in database for JWT validation'
- statusCode - 404

All other expections are covered in the route specific doc

# Auth /auth

## POST /register

**Description**

Used to create a new account

**Input**

Object with the properties:

- username - string, max length 50
- password - plain string, min length 8, max length 60
- profilePictureBase64 - string, optional, base64

**Output**

Object with the properties:

- uuid - string
- username - string
- password - bcrypt hashed string
- createdAt - timestamp in format YYYY-MM-DD HH:MM:SS
- modifiedAt - timestamp in format YYYY-MM-DD HH:MM:SS
- token - jwt signed token, use for further authentication

**Exceptions**

Gets returned when body, parameter or query validation failed

- Bad Request 400

    - statusCode - 400
    - error - 'Bad request'
    - message - Array with Objects inside

- Forbidden 403
    - statusCode

## POST /login

**Description**

Used to login to a account with the valid credentials

**Input**

Object with the properties:

- username - string, max length 50
- password - plain string, min length 8, max length 60

**Output**

token - jwt signed token, use for further authentication

**Exceptions**

Gets returned when body, parameter or query validation failed

- Bad Request 400

    - statusCode - 400
    - error - 'Bad request'
    - message - Array with Objects inside

Gets returned when no user with the credentials was found

- Not Found 404
    - message - 'Not found'
    - statusCode - 404

# Users /users

## GET /:uuid

**Description**

Gets a user with the specified uuid

**Input**

String param uuid; must be valid UUID;

Optional query with properties:

- withScores - boolean; specifies if scores should be retrieved too
- pageNum - positive integer; specifies the pageNum of the scores

**Output**

Object with properties:

- uuid - string
- username - string
- images - only if query is specified; object with properties:
    - data - array with the properties:
        - uuid - string
        - title - string
        - desc - string; can be null
        - upvotes - integer; can be null - stands for null
        - format - 'jpg' or 'png'
        - createdAt - timestamp in format YYYY-MM-DD HH:MM:SS
        - modifiedAt - timestamp in format YYYY-MM-DD HH:MM:SS
    - next - only if next page exists
    - previous - only if previous page exists
- createdAt - timestamp in format YYYY-MM-DD HH:MM:SS
- modifiedAt - timestamp in format YYYY-MM-DD HH:MM:SS

**Exceptions**

Gets returned when you set withScores query parameter to true but didn't supply pageNum

- Bad Request 400

    - message - 'When query param 'withScores' is set to true, you also need to set param 'pageNum''
    - statusCode - 400

Gets returned when no user was found in the database

- Not Found 404
    - message - 'Not found'
    - statusCode - 404

## GET /pages/:n

**Description**

Gets users by page and optionally with their scores

**Input**

Integer param n; must be positive;

Query with property 'date' set to 'newest' or 'oldest' **OR** property 'upvotes' set to 'most' or 'least'

**Output**

An Object with properties:

- users - Array of users with the properties:
    - uuid - string
    - username - string
    - password - bcrypt hashed string;
    - createdAt - timestamp in format YYYY-MM-DD HH:MM:SS
    - modifiedAt - timestamp in format YYYY-MM-DD HH:MM:SS
- next - only if next page exists; url
- previous - only if previous page exists; url

**Exceptions**

Gets returned when no query was specified

- Bad Request 400

    - message - 'Specify query'
    - statusCode - 400

Gets returned when no user was found

- Not Found 404
    - message - 'Not found'
    - statusCode - 404

# Images /images

## GET /high/:name

**Description**

Get the high res variant of the image by the specified name which must be in the format UUID.jpg

**Input**

String parameter name; must be valid UUID;

**Output**

Buffer representing the high res image to who the UUID belongs

**Exceptions**

Gets returned when the image was not found

- Not Found 404
    - message - 'Not Found'
    - statusCode - 404

## GET /low/:name

**Description**

Get the low res variant of the image by the specified name which must be in the format UUID.jpg

**Input**

String parameter name; must be valid UUID;

**Output**

Buffer representing the low res image to who the UUID belongs

**Exceptions**

Gets returned when the image was not found

- Not Found 404
    - message - 'Not Found'
    - statusCode - 404

## GET /profiles/high/:name

**Description**

Get the high res variant of the profile picture by the specified name which must be in the format UUID.jpg

**Input**

String parameter name; must be valid UUID;

**Output**

Buffer representing the high res profile picture to who the UUID belongs

**Exceptions**

Gets returned when the image was not found

- Not Found 404
    - message - 'Not Found'
    - statusCode - 404

## GET /profiles/low/:name

**Description**

Get the low res variant of the profile picture by the specified name which must be in the format UUID.jpg

**Input**

String parameter name; must be valid UUID;

**Output**

Buffer representing the low res profile picture to who the UUID belongs

**Exceptions**

Gets returned when the image was not found

- Not Found 404
    - message - 'Not Found'
    - statusCode - 404

## POST /

**Description**

Adds a new Image to a user

**Input**

Object with the properties:

- title - string, max length 100
- desc - optional, max length 65535
- base64 - optional, must be base64, represents the profile picture
- userUuid - the uuid of the user to who the image belongs
- format - format of the input image; can be jpg, jpeg or png

**Output**

Object with the properties:

- uuid - string
- title - string
- desc - string, optional
- upvotes - integer
- createdAt - timestamp in the format of YYYY-MM-DD HH:MM:SS
- modifiedAt - timestamp in the format of YYYY-MM-DD HH:MM:SS

**Exceptions**

Gets returned when body, parameter or query validation failed

- Bad Request 400

    - statusCode - 400
    - error - 'Bad request'
    - message - Array with Objects inside

Gets returned when someone tries to identify himself as another user

- Forbidden 403; proeprties:

    - message - 'You cannot add an image to another users account'
    - statusCode - 403

## PATCH /:uuid/upvotes

**Description**

Increments or decrements the upvotes of a image

**Input**

String parameter uuid; must be a valid uuid

Query with property increment set to true or decrement set to true

**Output**

1

**Exceptions**

Gets returned when body, parameter or query validation failed

- Bad Request 400

    - statusCode - 400
    - error - 'Bad request'
    - message - Array with Objects inside

Gets returned when you didn't supply increment or decrement query parameter

- Bad Request 400

    - message - 'Specify either increment or decrement query parameter'
    - statusCode - 400

Gets returned when you supplied both increment and decrement query parameter as true

- Bad Request 400
    - message - 'Specify either increment or decrement as true, not both'
    - statusCode - 400
