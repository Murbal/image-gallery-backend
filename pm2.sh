#!/bin/bash
pm2 kill
export PORT=3000
pm2 start dist/src/main.js --name="img3000"
export PORT=3001
pm2 start dist/src/main.js --name="img3001"
export PORT=3002
pm2 start dist/src/main.js --name="img3002"
export PORT=3003
pm2 start dist/src/main.js --name="img3003"
export PORT=3004
pm2 start dist/src/main.js --name="img3004"