import * as Knex from 'knex';

export async function up(knex: Knex, Promise): Promise<any> {
  if (!(await knex.schema.hasTable('users'))) {
    await knex.schema.createTable('users', (t: Knex.CreateTableBuilder) => {
      t.specificType('uuid', 'CHAR(36)')
        .primary()
        .notNullable();
      t.string('username', 50)
        .unique()
        .notNullable();
      t.specificType('password', 'CHAR(60)').notNullable();
      t.timestamp('created_at').notNullable();
      t.timestamp('modified_at').notNullable();
    });
  }
  if (!(await knex.schema.hasTable('images'))) {
    await knex.schema.createTable('images', (t: Knex.CreateTableBuilder) => {
      t.specificType('uuid', 'CHAR(36)')
        .primary()
        .notNullable();
      t.specificType('user_uuid', 'CHAR(36)').notNullable();
      t.string('title', 100)
        .notNullable()
        .index();
      t.text('desc').nullable();
      t.integer('upvotes')
        .index()
        .unsigned();
      t.specificType('format', 'CHAR(3)').notNullable();
      t.timestamp('createdAt')
        .index()
        .notNullable();
      t.timestamp('modifiedAt').notNullable();
      t.foreign('user_uuid').references('users.uuid');
    });
  }
  return new Promise((resolve, reject) => resolve());
}

export async function down(knex: Knex): Promise<any> {
  if (await knex.schema.hasTable('images')) {
    await knex.schema.dropTable('images');
  }
  if (await knex.schema.hasTable('users')) {
    await knex.schema.dropTable('users');
  }
  return new Promise((resolve, reject) => resolve());
}
