import { knexSnakeCaseMappers } from 'objection';

module.exports = {
  client: process.env.DB_CLIENT || 'mysql',
  connection: {
    host: process.env.DB_HOST || 'localhost',
    database: process.env.DB_DATABASE || 'image_gallery',
    user: process.env.DB_USER || 'root',
    password: process.env.DB_PASSWORD || 'TestPassword',
    dateStrings: true,
  },
  migrations: {
    tableName: 'knex_migrations',
  },
  pool: {
    min: 2,
    max: 30,
  },
  ...knexSnakeCaseMappers(),
};
